phog (0.1.7-1) unstable; urgency=medium

  * New upstream version

  [ Guido Günther ]
  * Use separate state file.
    When users switch between phog and other greeter configs
    they likely want the auto-login feature to retrigger so use a
    separate state file for that.
  * Add commented auto login session.
    This comes up ever so often. With this we can point users to the known
    working example.

  [ Arnaud Ferraris ]
  * d/watch: adapt to recent GitLab changes
  * d/control: make phosh-osk-stub the preferred OSK.
    See https://salsa.debian.org/DebianOnMobile-team/meta-phosh/-/merge_requests/38
  * debian: drop cherry-picked patch.
    It is now part of a formal release.
  * debian: drop no-longer-needed lintian overrides
  * d/copyright: drop entry for removed file
  * d/control: bump Standards-Version, no change required

 -- Arnaud Ferraris <aferraris@debian.org>  Mon, 09 Dec 2024 12:39:37 +0100

phog (0.1.6-3) unstable; urgency=medium

  * Team upload.
  * phog: Honor the systems default OSK

 -- Guido Günther <agx@sigxcpu.org>  Sun, 09 Jun 2024 09:21:41 +0200

phog (0.1.6-2) unstable; urgency=medium

  * d/control: depend on `librsvg2-common`
    This package is needed to get the GDK pixbuf loader plugin for SVG; it
    is usually pulled in by GNOME-based desktop environments, but users
    choosing a different DE (e.g. KDE Plasma) will lack this package,
    leading to `phog` being unable to load SVG icons.

 -- Arnaud Ferraris <aferraris@debian.org>  Sat, 20 Jan 2024 12:10:21 +0100

phog (0.1.6-1) unstable; urgency=medium

  * New upstream version

  [ Guido Günther ]
  * d/control: Bump phoc dependency.
    We want one supporting ext-idle-notify-v1

  [ Arnaud Ferraris ]
  * d/copyright: remove now-uneeded paragraph

 -- Arnaud Ferraris <aferraris@debian.org>  Fri, 17 Nov 2023 16:02:43 +0100

phog (0.1.5-1) unstable; urgency=medium

  * New upstream version

 -- Arnaud Ferraris <aferraris@debian.org>  Fri, 06 Oct 2023 13:27:21 +0200

phog (0.1.4-1) unstable; urgency=medium

  * New upstream version 0.1.4
  * d/patches: drop obsolete patch.
    This was needed for greetd v0.8, but we now have v0.9 in Debian, so
    let's drop that one.
  * d/control: bump Standards-Version, no changes needed
  * d/lintian-overrides: fix override

 -- Arnaud Ferraris <aferraris@debian.org>  Thu, 21 Sep 2023 19:05:55 +0200

phog (0.1.3-2) unstable; urgency=medium

  [ Guido Günther ]
  * d/control: Allow for phosh-osk-stub.
    This allows to use phosh-osk-stub instead of squeekboard

  [ Arnaud Ferraris ]
  * debian: remove PAM conffiles on upgrade
    `greetd` now ships those, so we should get rid of them. Add a versioned
    dependency on `greetd` to ensure we have the proper configs.
    (Closes: #1032914)
  * d/control: add missing runtime dependencies.
    (Closes: #1033282)

 -- Arnaud Ferraris <aferraris@debian.org>  Wed, 15 Mar 2023 13:59:03 +0100

phog (0.1.3-1) unstable; urgency=medium

  * New upstream version 0.1.3

 -- Arnaud Ferraris <aferraris@debian.org>  Thu, 02 Feb 2023 19:50:11 +0100

phog (0.1.2-1) unstable; urgency=medium

  * New upstream version 0.1.2

 -- Arnaud Ferraris <aferraris@debian.org>  Fri, 25 Nov 2022 17:57:01 +0100

phog (0.1.1-1) unstable; urgency=medium

  * New upstream version 0.1.1
  * d/patches: rebase on new upstream release
  * debian: create working directory and ensure proper permissions
    `phog` needs to store data into a folder named `/var/lib/phog`. As this
    folder is not under the user's home directory, we must create it and
    ensure the user `phog` will run as (`_greetd` here) has R/W permissions
    on it.

 -- Arnaud Ferraris <aferraris@debian.org>  Thu, 03 Nov 2022 18:00:37 +0100

phog (0.1.0-1) unstable; urgency=medium

  * Initial Debian packaging (Closes: #1022904)

 -- Arnaud Ferraris <aferraris@debian.org>  Thu, 27 Oct 2022 17:07:20 +0200
