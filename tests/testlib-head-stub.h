/*
 * Copyright (C) 2021 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#include "phog-wayland.h"

#include <glib.h>

G_BEGIN_DECLS

void phog_test_head_stub_init        (PhogWayland *wl);
void phog_test_head_stub_destroy     (void);

G_END_DECLS
