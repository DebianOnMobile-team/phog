/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* Stubs so we don't need to run the shell */

#include <glib.h>
#include "phog-wayland.h"
#include "shell.h"

PhogToplevelManager *toplevel_manager = NULL;

PhogShell*
phog_shell_get_default (void)
{
  return NULL;
}

void
phog_shell_get_usable_area (PhogShell *self, int *x, int *y, int *width, int *height)
{
  if (x)
    *x = 16;
  if (y)
    *y = 32;
  if (width)
    *width = 64;
  if (height)
    *height = 128;
  return;
}

PhogMonitor *
phog_shell_get_primary_monitor (PhogShell *self)
{
  return NULL;
}

PhogToplevelManager*
phog_shell_get_toplevel_manager (PhogShell *self)
{
  return g_object_new (PHOG_TYPE_TOPLEVEL_MANAGER, NULL);
}

void
phog_shell_set_state (PhogShell *self, guint state, gboolean set)
{
}

guint
phog_shell_get_state (PhogShell *self)
{
  return 0;
}

gboolean
phog_shell_get_locked (PhogShell *self)
{
  return FALSE;
}

PhogLockscreenManager *
phog_shell_get_lockscreen_manager (PhogShell *self)
{
  return NULL;
}
