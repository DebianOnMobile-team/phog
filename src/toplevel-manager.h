/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "toplevel.h"

#define PHOG_TYPE_TOPLEVEL_MANAGER (phog_toplevel_manager_get_type())

G_DECLARE_FINAL_TYPE (PhogToplevelManager,
                      phog_toplevel_manager,
                      PHOG,
                      TOPLEVEL_MANAGER,
                      GObject)

PhogToplevel        *phog_toplevel_manager_get_toplevel (PhogToplevelManager *self, guint num);
guint                 phog_toplevel_manager_get_num_toplevels (PhogToplevelManager *self);
PhogToplevelManager *phog_toplevel_manager_new (void);
