/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define PHOG_TYPE_OSK_BUTTON (phog_osk_button_get_type())

G_DECLARE_FINAL_TYPE (PhogOskButton, phog_osk_button, PHOG, OSK_BUTTON, GtkToggleButton)

GtkWidget * phog_osk_button_new (void);

G_END_DECLS
