/*
 * Copyright © 2019 Alexander Mikhaylenko <alexm@gnome.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gtk/gtk.h>

#pragma once

G_BEGIN_DECLS

#define PHOG_TYPE_ARROW (phog_arrow_get_type())

G_DECLARE_FINAL_TYPE (PhogArrow, phog_arrow, PHOG, ARROW, GtkDrawingArea)

PhogArrow *phog_arrow_new (void);

double      phog_arrow_get_progress (PhogArrow *self);
void        phog_arrow_set_progress (PhogArrow *self,
                                      double      progress);

G_END_DECLS
