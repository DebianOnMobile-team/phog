/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include <glib-object.h>
#include "phog-wwan-iface.h"
#include "wwan-manager.h"

G_BEGIN_DECLS

#define PHOG_TYPE_WWAN_MM (phog_wwan_mm_get_type())

G_DECLARE_FINAL_TYPE (PhogWWanMM, phog_wwan_mm, PHOG, WWAN_MM, PhogWWanManager)

PhogWWanMM * phog_wwan_mm_new                (void);

G_END_DECLS
