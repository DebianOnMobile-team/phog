/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

/* Wifi Info widget */

#define G_LOG_DOMAIN "phog-wifiinfo"

#include "phog-config.h"

#include "shell.h"
#include "wifiinfo.h"

/**
 * SECTION:wifiinfo
 * @short_description: A widget to display the wifi status
 * @Title: PhogWifiInfo
 *
 * #PhogWifiInfo displays the current wifi status based on information
 * from #PhogWifiManager. To figure out if the widget should be shown
 * the #PhogWifiInfo:enabled property can be useful.
 */

enum {
  PROP_0,
  PROP_ENABLED,
  PROP_PRESENT,
  PROP_LAST_PROP
};
static GParamSpec *props[PROP_LAST_PROP];

struct _PhogWifiInfo {
  PhogStatusIcon parent;

  gboolean          enabled;
  gboolean          present;
  PhogWifiManager *wifi;
};
G_DEFINE_TYPE (PhogWifiInfo, phog_wifi_info, PHOG_TYPE_STATUS_ICON);

static void
phog_wifi_info_get_property (GObject    *object,
                              guint       property_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  PhogWifiInfo *self = PHOG_WIFI_INFO (object);

  switch (property_id) {
  case PROP_ENABLED:
    g_value_set_boolean (value, self->enabled);
    break;
  case PROP_PRESENT:
    g_value_set_boolean (value, self->present);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
update_icon (PhogWifiInfo *self, GParamSpec *pspec, PhogWifiManager *wifi)
{
  const char *icon_name;

  g_debug ("Updating wifi icon");
  g_return_if_fail (PHOG_IS_WIFI_INFO (self));
  g_return_if_fail (PHOG_IS_WIFI_MANAGER (wifi));

  icon_name = phog_wifi_manager_get_icon_name (wifi);
  if (icon_name)
    phog_status_icon_set_icon_name (PHOG_STATUS_ICON (self), icon_name);
}

static void
update_info (PhogWifiInfo *self)
{
  const char *info;
  g_return_if_fail (PHOG_IS_WIFI_INFO (self));

  info = phog_wifi_manager_get_ssid (self->wifi);
  if (info)
    phog_status_icon_set_info (PHOG_STATUS_ICON (self), info);
  else
    phog_status_icon_set_info (PHOG_STATUS_ICON (self), _("Wi-Fi"));
}

static void
on_wifi_enabled (PhogWifiInfo *self, GParamSpec *pspec, PhogWifiManager *wifi)
{
  gboolean enabled;

  g_debug ("Updating wifi status");
  g_return_if_fail (PHOG_IS_WIFI_INFO (self));
  g_return_if_fail (PHOG_IS_WIFI_MANAGER (wifi));

  enabled = phog_wifi_manager_get_enabled (wifi);
  if (self->enabled == enabled)
    return;

  self->enabled = enabled;
  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_ENABLED]);
}

static void
on_wifi_present (PhogWifiInfo *self, GParamSpec *pspec, PhogWifiManager *wifi)
{
  gboolean present;

  g_return_if_fail (PHOG_IS_WIFI_INFO (self));
  g_return_if_fail (PHOG_IS_WIFI_MANAGER (wifi));

  present = phog_wifi_manager_get_present (wifi);
  if (self->present == present)
    return;

  self->present = present;
  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_PRESENT]);
}


static void
phog_wifi_info_idle_init (PhogStatusIcon *icon)
{
  PhogWifiInfo *self = PHOG_WIFI_INFO (icon);

  update_icon (self, NULL, self->wifi);
  update_info (self);
  on_wifi_enabled (self, NULL, self->wifi);
}


static void
phog_wifi_info_constructed (GObject *object)
{
  PhogWifiInfo *self = PHOG_WIFI_INFO (object);
  PhogShell *shell;

  G_OBJECT_CLASS (phog_wifi_info_parent_class)->constructed (object);

  shell = phog_shell_get_default();
  self->wifi = g_object_ref(phog_shell_get_wifi_manager (shell));

  if (self->wifi == NULL) {
    g_warning ("Failed to get wifi manager");
    return;
  }

  g_signal_connect_swapped (self->wifi,
                            "notify::icon-name",
                            G_CALLBACK (update_icon),
                            self);

  g_signal_connect_swapped (self->wifi,
                            "notify::ssid",
                            G_CALLBACK (update_info),
                            self);

  /* We don't use a binding for self->enabled so we can keep
     the property r/o */
  g_signal_connect_swapped (self->wifi,
                            "notify::enabled",
                            G_CALLBACK (on_wifi_enabled),
                            self);
  on_wifi_enabled (self, NULL, self->wifi);
  g_signal_connect_swapped (self->wifi,
                            "notify::present",
                            G_CALLBACK (on_wifi_present),
                            self);
  on_wifi_present (self, NULL, self->wifi);
}


static void
phog_wifi_info_dispose (GObject *object)
{
  PhogWifiInfo *self = PHOG_WIFI_INFO(object);

  if (self->wifi) {
    g_signal_handlers_disconnect_by_data (self->wifi, self);
    g_clear_object (&self->wifi);
  }

  G_OBJECT_CLASS (phog_wifi_info_parent_class)->dispose (object);
}


static void
phog_wifi_info_class_init (PhogWifiInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  PhogStatusIconClass *status_icon_class = PHOG_STATUS_ICON_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = phog_wifi_info_constructed;
  object_class->dispose = phog_wifi_info_dispose;
  object_class->get_property = phog_wifi_info_get_property;

  status_icon_class->idle_init = phog_wifi_info_idle_init;

  gtk_widget_class_set_css_name (widget_class, "phog-wifi-info");

  props[PROP_ENABLED] =
    g_param_spec_boolean ("enabled",
                          "enabled",
                          "Whether a wifi device is enabled",
                          FALSE,
                          G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS |
                          G_PARAM_EXPLICIT_NOTIFY);
  props[PROP_PRESENT] =
    g_param_spec_boolean ("present",
                          "Present",
                          "Whether wifi hardware is present",
                          FALSE,
                          G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS |
                          G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);
}


static void
phog_wifi_info_init (PhogWifiInfo *self)
{
}


GtkWidget *
phog_wifi_info_new (void)
{
  return g_object_new (PHOG_TYPE_WIFI_INFO, NULL);
}
