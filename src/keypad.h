/*
 * Copyright (C) 2021 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define PHOG_TYPE_KEYPAD (phog_keypad_get_type ())

G_DECLARE_FINAL_TYPE (PhogKeypad, phog_keypad, PHOG, KEYPAD, GtkGrid)

GtkWidget *phog_keypad_new                     (void);
void       phog_keypad_set_entry               (PhogKeypad *self,
                                                 GtkEntry    *entry);
GtkEntry  *phog_keypad_get_entry               (PhogKeypad *self);
void       phog_keypad_set_start_action        (PhogKeypad *self,
                                                 GtkWidget   *start_action);
GtkWidget *phog_keypad_get_start_action        (PhogKeypad *self);
void       phog_keypad_set_end_action          (PhogKeypad *self,
                                                 GtkWidget   *end_action);
GtkWidget *phog_keypad_get_end_action          (PhogKeypad *self);
void       phog_keypad_set_shuffle             (PhogKeypad *self,
                                                 gboolean     shuffle);
gboolean   phog_keypad_get_shuffle             (PhogKeypad *self);
void       phog_keypad_distribute              (PhogKeypad *self);
G_END_DECLS
