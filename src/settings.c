/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phog-settings"

#include <glib/gi18n.h>

#include "shell.h"
#include "settings.h"
#include "settings/brightness.h"
#include "wwan/wwan-manager.h"

#include <gio/gdesktopappinfo.h>
#include <xkbcommon/xkbcommon.h>

#include <math.h>

#define STACK_CHILD_NOTIFICATIONS    "notifications"
#define STACK_CHILD_NO_NOTIFICATIONS "no-notifications"

/**
 * SECTION:settings
 * @short_description: The settings menu
 * @Title: PhogSettings
 */
enum {
  PROP_0,
  PROP_ON_LOCKSCREEN,
  PROP_DRAG_HANDLE_OFFSET,
  PROP_LAST_PROP,
};
static GParamSpec *props[PROP_LAST_PROP];

enum {
  SETTING_DONE,
  N_SIGNALS
};
static guint signals[N_SIGNALS] = { 0 };

typedef struct _PhogSettings
{
  GtkBin parent;

  gboolean   on_lockscreen;
  gint       drag_handle_offset;
  guint      debounce_handle;

  GtkWidget *box_settings;
  GtkWidget *scale_brightness;
} PhogSettings;


G_DEFINE_TYPE (PhogSettings, phog_settings, GTK_TYPE_BIN)


static void
phog_settings_set_property (GObject *object,
                             guint property_id,
                             const GValue *value,
                             GParamSpec *pspec)
{
  PhogSettings *self = PHOG_SETTINGS (object);

  switch (property_id) {
  case PROP_ON_LOCKSCREEN:
    self->on_lockscreen = g_value_get_boolean (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
phog_settings_get_property (GObject *object,
                             guint property_id,
                             GValue *value,
                             GParamSpec *pspec)
{
  PhogSettings *self = PHOG_SETTINGS (object);

  switch (property_id) {
  case PROP_ON_LOCKSCREEN:
    g_value_set_boolean (value, self->on_lockscreen);
    break;
  case PROP_DRAG_HANDLE_OFFSET:
    g_value_set_int (value, self->drag_handle_offset);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
calc_drag_handle_offset (PhogSettings *self)
{
  gint h = gtk_widget_get_allocated_height (GTK_WIDGET (self));

  if (self->drag_handle_offset == h)
    return;

  self->drag_handle_offset = h;
  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_DRAG_HANDLE_OFFSET]);
}


static gboolean
delayed_update_drag_handle_offset (gpointer data)
{
  PhogSettings *self = PHOG_SETTINGS (data);

  self->debounce_handle = 0;
  calc_drag_handle_offset (self);
  return G_SOURCE_REMOVE;
}


static void
update_drag_handle_offset (PhogSettings *self)
{
  g_clear_handle_id (&self->debounce_handle, g_source_remove);
  self->debounce_handle = g_timeout_add (200, delayed_update_drag_handle_offset, self);
  g_source_set_name_by_id (self->debounce_handle, "[phog] delayed_update_drag_handle_offset");
}


static void
brightness_value_changed_cb (GtkScale *scale_brightness, gpointer unused)
{
  int brightness;

  brightness = (int)gtk_range_get_value (GTK_RANGE (scale_brightness));
  brightness_set (brightness);
}


static void
setup_brightness_range (PhogSettings *self)
{
  gtk_range_set_range (GTK_RANGE (self->scale_brightness), 0, 100);
  gtk_range_set_round_digits (GTK_RANGE (self->scale_brightness), 0);
  gtk_range_set_increments (GTK_RANGE (self->scale_brightness), 1, 10);
  brightness_init (GTK_SCALE (self->scale_brightness));
  g_signal_connect (self->scale_brightness,
                    "value-changed",
                    G_CALLBACK(brightness_value_changed_cb),
                    NULL);
}


static void
phog_settings_constructed (GObject *object)
{
  PhogSettings *self = PHOG_SETTINGS (object);

  setup_brightness_range (self);

  g_object_bind_property (phog_shell_get_default (),
                          "locked",
                          self,
                          "on-lockscreen",
                          G_BINDING_SYNC_CREATE);

  G_OBJECT_CLASS (phog_settings_parent_class)->constructed (object);
}


static void
phog_settings_dispose (GObject *object)
{
  brightness_dispose ();

  G_OBJECT_CLASS (phog_settings_parent_class)->dispose (object);
}


static void
phog_settings_finalize (GObject *object)
{
  PhogSettings *self = PHOG_SETTINGS (object);

  g_clear_handle_id (&self->debounce_handle, g_source_remove);

  G_OBJECT_CLASS (phog_settings_parent_class)->finalize (object);
}



static void
phog_settings_class_init (PhogSettingsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = phog_settings_dispose;
  object_class->finalize = phog_settings_finalize;
  object_class->constructed = phog_settings_constructed;
  object_class->set_property = phog_settings_set_property;
  object_class->get_property = phog_settings_get_property;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/mobian/phog/ui/settings.ui");

  /* PhogSettings:on-lockscreen:
   *
   * Whether settings are shown on lockscreen (%TRUE) or in the unlocked shell
   * (%FALSE).
   */
  props[PROP_ON_LOCKSCREEN] =
    g_param_spec_boolean (
      "on-lockscreen", "", "",
      FALSE,
      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  /* PhogSettings:handle-offset:
   *
   * The offset from the top of the widget where it's safe to start
   * dragging. Seep hosh_settings_get_drag_drag_handle_offset().
   */
  props[PROP_DRAG_HANDLE_OFFSET] =
    g_param_spec_int (
      "drag-handle-offset", "", "",
      0,
      G_MAXINT,
      0,
      G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);

  signals[SETTING_DONE] = g_signal_new ("setting-done",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
      NULL, G_TYPE_NONE, 0);

  gtk_widget_class_bind_template_child (widget_class, PhogSettings, box_settings);
  gtk_widget_class_bind_template_child (widget_class, PhogSettings, scale_brightness);

  gtk_widget_class_bind_template_callback (widget_class, update_drag_handle_offset);
}


static void
phog_settings_init (PhogSettings *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


GtkWidget *
phog_settings_new (void)
{
  return g_object_new (PHOG_TYPE_SETTINGS, NULL);
}

/**
 * phog_settings_get_drag_handle_offset:
 * @self: The settings
 *
 * Get the y coordinate from the top of the widget where dragging
 * can start. E.g. we don't want drag to work on notifications as
 * notifications need to scroll in vertical direction.
 *
 * Returns: The y coordinate at which dragging the surface can start.
 */
gint
phog_settings_get_drag_handle_offset (PhogSettings *self)
{
  g_return_val_if_fail (PHOG_IS_SETTINGS (self), 0);

  return self->drag_handle_offset;
}
